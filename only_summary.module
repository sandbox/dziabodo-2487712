<?php

/**
 * Parses and verifies the doc comments for files.
 *
 * PHP version 5
 *
 * @category PHP
 * @package  Only_Summary
 * @link     http://blueok.co
 */


/**
 * Implements hook_field_formatter_info().
 */
function only_summary_field_formatter_info() {
  return array(
    'only_summary' => array(
      'label' => t('Only summary'),
      'field types' => array('text_with_summary'),
      'settings' => array(
        'more_link' => TRUE,
        'more_text' => t('Read more'),
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function only_summary_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  $extension = '';
  $uri = '';
  $output = '';
  $uri = entity_uri($entity_type, $entity);
  if ($uri && $settings['more_link'] && strpos(strrev($output), strrev('<!--break-->')) !== 0) {
    $extension .= l($settings['more_text'], $uri['path'], array('html' => TRUE, 'attributes' => array('class' => array('more-link'))));
  }
  foreach ($items as $delta => $item) {
    $element[$delta] = array(
      '#theme' => 'only_summary',
      '#instance' => $instance,
      '#langcode' => $langcode,
      '#more_link' => $extension,
      '#item' => $item,

    );

  }
  return $element;
}

/**
 * Implements hook_theme().
 */
function only_summary_theme() {
  return array(
    'only_summary' => array(
      'variables' => array(
        'instance' => NULL,
        'langcode' => NULL,
        'more_link' => NULL,
        'item' => NULL,
      ),
      'template' => 'only_summary',
    ),
  );
}

/**
 * Process variables for tpl file.
 */
function template_preprocess_only_summary(&$variables) {
  if (!empty($variables['item']['summary'])) {
    $variables['safe_summary'] = _text_sanitize($variables['instance'], $variables['langcode'], $variables['item'], 'summary');
  }

}


/**
 * Implements hook_field_formatter_settings_form().
 */
function only_summary_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

  $settings = $instance['display'][$view_mode]['settings'];
  $form = array();
  $form['more_link'] = array(
    '#title' => t('Display more link?'),
    '#type' => 'select',
    '#options' => array(
      0 => t("No"),
      1 => t("Yes"),
    ),
    '#default_value' => $settings['more_link'],
    '#description' => t('Displays a link to the entity (if one exists)'),
  );
  $form['more_text'] = array(
    '#title' => t('More link text'),
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => $settings['more_text'],
    '#description' => t('If displaying more link, enter the text for the link.'),
  );
  return $form;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function only_summary_field_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = t('More Link');
  return $summary;
}
